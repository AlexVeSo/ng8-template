const app = require('./app');
const http = require('http');

const debug = require('debug')('node-angular');

const normalizePort = val => {
  const portValue = parseInt(val, 10);

  if (isNaN(portValue)) {
    return val;
  }

  if (portValue >= 0) {
    return portValue;
  }

  return false;
};

const port = normalizePort(process.env.PORT || 3000);

const onError = error => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `pipe ${port}` : `port ${port}`;
  switch (error.code) {
    case 'EACCESS':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const server = http.createServer(app);

const onListening = () => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${port}`;
  console.log(`Listening on ${bind}`);
  debug(`Listening on ${bind}`);
};

app.set('port', port);

server.on('error', onError);
server.on('listening', onListening);
server.listen(port);
