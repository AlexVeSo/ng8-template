const PostModel = require('../models/example.post.model');

exports.createPost = (req, res, next) => {
  const url = `${req.protocol}://${req.get('host')}`;
  const post = new PostModel({
    title: req.body.title,
    content: req.body.content,
    imagePath: `${url}/images/${req.userData.userId}/${req.file.filename}`,
    creator: req.userData.userId
  });
  post.save().then(result => {
    res.status(201).json({
      message: 'Post added successfully!',
      post: {
        ...post,
        id: result._id
      }
    });
  })
    .catch(error => {
      res.status(500).json({ message: 'Created a post failed!' });
    });
}

exports.updatePost = (req, res, next) => {
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = `${req.protocol}://${req.get('host')}`;
    imagePath = `${url}/images/${req.userData.userId}/${req.file.filename}`
  }
  const post = {
    title: req.body.title,
    content: req.body.content,
    imagePath: imagePath,
    creator: req.userData.userId
  };
  PostModel.updateOne({ _id: req.params.id, creator: req.userData.userId }, post).then((result) => {
    if (result.n > 0) {
      res.status(200).json({
        message: 'Update successfull!'
      });
    } else {
      res.status(401).json({
        message: 'Not authorized!'
      });
    }
  })
    .catch(error => {
      res.status(500).json({ message: 'Couldn\'t update post!' });
    });
};

exports.getPosts = (req, res, next) => {
  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;
  const postQuery = PostModel.find();
  let queryData;
  if (pageSize && currentPage) {
    postQuery
      .skip(pageSize * (currentPage - 1))
      .limit(pageSize);
  }
  postQuery.then(documents => {
    queryData = documents
    return PostModel.countDocuments();
  }).then(count => {
    res.status(200).json({
      message: 'Posts fetched successfully!',
      data: queryData,
      maxPosts: count
    });
  })
    .catch(error => {
      res.status(500).json({ message: 'Fetching posts failed!' });
    });
};

exports.getPost = (req, res, next) => {
  PostModel.findById(req.params.id).then(post => {
    if (post) {
      res.status(200).json({ message: 'Post found successfully!', data: post });
    } else {
      res.status(404).json({ message: 'Post not found!' });
    }
  })
    .catch(error => {
      res.status(500).json({ message: 'Fetching post failed!' });
    });;
};

exports.deletePost = (req, res, next) => {
  PostModel.deleteOne({ _id: req.params.id, creator: req.userData.userId }).then((result) => {
    if (result.n > 0) {
      res.status(200).json({ message: 'Post deleted!' });
    } else {
      res.status(401).json({ message: 'Not authorized!' });
    }
  })
    .catch(error => {
      res.status(500).json({ message: 'Deleting post failed!' });
    });;
};
