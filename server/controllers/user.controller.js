const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userModel = require('../models/user.model');

exports.createUser = (req, res, next) => {
  bcrypt.hash(req.body.password, 10)
    .then(hash => {
      const user = new userModel({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: hash
      });

      user.save()
        .then(result => {
          res.status(200).json({
            message: 'User created!',
            result: result
          });
        })
        .catch(err => {
          res.status(500).json({ message: "Invalid authentication credentials!" });
        });
    });
}

exports.loginUser = (req, res, next) => {
  let fetchedUser;
  userModel.findOne({ email: req.body.email })
    .then(user => {
      if (!user) {
        return res.status(401).json({ message: 'User not found!' });
      }
      fetchedUser = user;
      return bcrypt.compare(req.body.password, user.password);
    })
    .then(result => {
      if (!result) {
        return res.status(401).json({ message: 'Wrong password!' });
      }
      const token = jwt.sign({
        email: fetchedUser.email,
        userId: fetchedUser._id,
        firstname: fetchedUser.firstname,
        lastname: fetchedUser.lastname
      }, 'some_very_long_secret', {
        expiresIn: '1h'
      });
      res.status(200).json({
        message: 'LoggedIn successfull!',
        token,
        expiresIn: 3600,
        userId: fetchedUser._id
      });
    })
    .catch(err => {
      console.log(err);
      return res.status(401).json({ message: 'Invalid authentication credentials!' });
    });
}
