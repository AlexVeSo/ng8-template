const express = require('express');
const router = express.Router();

const checkAuth = require('../middleware/check-auth');
const extractImage = require('../middleware/image-upload');
const PostController = require('../controllers/example.post.controller');

router.post('', checkAuth, extractImage, PostController.createPost);

router.put('/:id', checkAuth, extractImage, PostController.updatePost);

router.get('', PostController.getPosts);

router.get('/:id', PostController.getPost);

router.delete('/:id', checkAuth, PostController.deletePost);

module.exports = router;
