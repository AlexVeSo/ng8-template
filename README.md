# Ng8Template

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.0.

## Requirements

- Docker

## Initial setup

Run `npm run mongo:initialize` to setup a docker container with mongodb.

## Development server

Run `npm start` for a dev server. Additionally you could run `ng serve` if you are not using the server part. Navigate to `http://localhost:4200`.
The app will automatically reload if you change any of the source files. The server can be accessed from both `http://localhost:4200/api` and `http://localhost:3000/api`.

We are using the built in proxy of Angular to avoid the cross browsing error (CORS).

## Additional commands

- Run `npm run mongo:start` to start MongoDB.
- Run `npm run mongo:stop` to stop MongoDB, you will need to stop your server, if you plan to start a new instance/project.
- Run `npm run mongo:destroy` to cleanup you MongoDB, this will stop your server, and remove it from your docker repository.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
