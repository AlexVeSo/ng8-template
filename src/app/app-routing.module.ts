import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './auth/auth.guard';

import { PublicComponent } from './pages/public/public.component';
import { SecureComponent } from './pages/secure/secure.component';

const routes: Routes = [
  { path: 'home', component: PublicComponent },
  { path: 'secure', component: SecureComponent, canActivate: [ AuthGuard ] },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
  { path: 'posts', loadChildren: './posts/posts.module#PostsModule', canActivate: [AuthGuard] },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
