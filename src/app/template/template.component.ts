import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MatDrawer } from '@angular/material';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})
export class TemplateComponent implements OnInit, OnDestroy {

  @Input() title: string;
  userIsAuthenticated = false;
  private authSubscription: Subscription;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe([Breakpoints.Handset]).pipe(
    map(result => result.matches),
    shareReplay()
  );

  constructor(
    private authService: AuthService,
    private breakpointObserver: BreakpointObserver
  ) { }

  ngOnInit() {
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authSubscription = this.authService.getAuthStatusListener()
      .subscribe(isAuthenticated => this.userIsAuthenticated = isAuthenticated);
  }

  onLogout(drawer: MatDrawer) {
    if (drawer) {
      drawer.close();
    }
    this.authService.logout();
  }

  ngOnDestroy() {
    this.authSubscription.unsubscribe();
  }
}
