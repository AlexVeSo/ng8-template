import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorComponent } from './error.component';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  durationInSeconds = 5;

  constructor(
    private snackbar: MatSnackBar
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        let errMsg = 'An unknown error occured!!';
        if (error.error.message) {
          errMsg = error.error.message;
        }
        this.snackbar.openFromComponent(ErrorComponent, {
          duration: this.durationInSeconds * 1000,
          data: { message: errMsg },
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
        return throwError(error);
      })
    );
  }
}
