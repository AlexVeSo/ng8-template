import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Post } from './post';

import { environment } from '../../environments/environment';

interface PostGet extends Post {
  _id: string;
}

interface PostRequest {
  message: string;
  data: PostGet[];
  maxPosts: number;
}

@Injectable({
  providedIn: 'root'
})
export class PostService {

  apiUrl = environment.apiUrl;
  private posts: Post[] = [];
  private postsUpdated = new Subject<{ posts: Post[], postCount: number }>();

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  getPosts(pageSize: number, page: number) {
    const queryParams = `?pagesize=${pageSize}&page=${page}`;
    return this.http.get<PostRequest>(`${this.apiUrl}/posts${queryParams}`).pipe(
      map((postData) => {
        return {
          posts: postData.data.map((post: PostGet) => {
            return {
              title: post.title,
              content: post.content,
              id: post._id,
              imagePath: post.imagePath,
              creator: post.creator
            };
          }),
          maxPosts: postData.maxPosts
        };
      })
    ).subscribe((responseData) => {
      this.posts = responseData.posts;
      this.postsUpdated.next({ posts: [...this.posts], postCount: responseData.maxPosts });
    });
  }

  getPostUpdatedListener() {
    return this.postsUpdated.asObservable();
  }

  getPost(postId: string): Observable<{ data: PostGet }> {
    return this.http.get<{ data: PostGet }>(`${this.apiUrl}/posts/${postId}`);
  }

  addPost(title: string, content: string, image: File) {
    const postData = new FormData();
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image, title);

    this.http.post<{ message: string, post: Post }>(`${this.apiUrl}/posts`, postData).subscribe((response) => {
      this.router.navigate(['/posts']);
    });
  }

  updatePost(id: string, title: string, content: string, image: File | string) {
    let postData: Post | FormData;
    if (typeof(image) === 'object') {
      postData = new FormData();
      postData.append('id', id);
      postData.append('title', title);
      postData.append('content', content);
      postData.append('image', image, title);
    } else {
      postData = { id, title, content, imagePath: image, creator: null };
    }

    this.http.put(`${this.apiUrl}/posts/${id}`, postData).subscribe((response) => {
      this.router.navigate(['/posts']);
    });
  }

  deletePost(postId: string) {
    return this.http.delete(`${this.apiUrl}/posts/${postId}`);
  }
}
