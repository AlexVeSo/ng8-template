import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatInputModule,
  MatToolbarModule,
  MatCardModule,
  MatButtonModule,
  MatExpansionModule,
  MatProgressSpinnerModule,
  MatPaginatorModule,
  MatIconModule,
  MatDialogModule,
  MatSidenavModule,
  MatListModule,
  MatSnackBarModule,
  MatMenuModule,
  MatGridListModule
} from '@angular/material';

const modules = [
  FlexLayoutModule,
  MatInputModule,
  MatToolbarModule,
  MatCardModule,
  MatButtonModule,
  MatExpansionModule,
  MatProgressSpinnerModule,
  MatPaginatorModule,
  MatIconModule,
  MatDialogModule,
  MatSidenavModule,
  MatListModule,
  MatSnackBarModule,
  MatMenuModule,
  MatGridListModule
];

@NgModule({
  imports: [modules],
  exports: [modules]
})
export class AppMaterialModule { }
