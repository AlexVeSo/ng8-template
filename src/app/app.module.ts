// Angular imports
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Other imports
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';

// import custom modules
import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';
import { TemplateModule } from './template/template.module';

// Import Interceptors
import { AuthInterceptor } from './auth/auth.interceptor';
import { ErrorInterceptor } from './template/error/error.interceptor';

// import components
import { AppComponent } from './app.component';
import { ErrorComponent } from './template/error/error.component';
import { PublicComponent } from './pages/public/public.component';
import { SecureComponent } from './pages/secure/secure.component';
import { ShibaComponent } from './pages/secure/shiba/shiba.component';


@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    PublicComponent,
    SecureComponent,
    ShibaComponent
  ],
  imports: [
    BrowserModule,
    TemplateModule,
    HttpClientModule,
    AppRoutingModule,
    AppMaterialModule,
    SlimLoadingBarModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ErrorComponent]
})
export class AppModule { }
